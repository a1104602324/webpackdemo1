# webpack脚手架构建完整前端项目

#### 介绍
webpack脚手架构建完整前端项目

#### 软件架构
1.需要安装的插件：
    "clean-webpack-plugin": "^3.0.0",
    "css-loader": "^3.0.0",
    "extract-text-webpack-plugin": "^3.0.2",
    "html-loader": "^0.5.5",
    "html-webpack-plugin": "^3.2.0",
    "style-loader": "^0.23.1",
    "webpack": "^3.5.4"
    "html-webpack-plugin-for-multihtml": "^2.30.2"

#### 安装教程

1. 把项目克隆到本地： git clone https://gitee.com/a1104602324/webpackdemo1.git
2. 用编译器打开项目，用npm/cnpm初始化项目: npm init / cnpm init 
3. 用webpack构建项目： 先全局安装，再项目内安装


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 联系我

1. 添加 QQ1104602324 一起参与前后端服务器架构等技术讨论（Java、Go、Py等）