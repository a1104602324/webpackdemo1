/**
 * 1. 已经全局安装过3.0.0 的webpack ， 进入项目内先npm init 项目初始化 
 * 2. 执行cnpm / npm install webpack@3.0.0 --save-dev 安装本项目的webpack（固定全局版本）
 * 3. 在根目录新建一个webpack.config.js文件 , 配置基本入口和出口等
 * 4. 安装各种插件-cnpm 安装即可：
 * webpack,webpack-cli,
 * html-webpack-plugin, 
 * extract-text-webpack-plugin,
 * 'clean-webpack-plugin,
 * style-loader,css-loader,
 * url-loader,
 * file-loader,
 * html-loader,
 * css-loader
 * 
 * 5. webpack.config.js文件中引入需要的插件,package.json 中 script加入：  "build": "webpack"
 * 6. 把样式文件引入相应的js中 ： require or import
 * 7. 执行cnpm run build 进行打包本地调试
 * 
 * 常用语句：
 * js中 ： require("webpack")  - > 引入js文件
 */


require('../css/index.css');
console.log('this is index js page');