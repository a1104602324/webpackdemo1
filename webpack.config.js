const path = require('path');//引入node中的path模块
var HtmlWebpackPlugin = require("html-webpack-plugin");
//引入html-webpack-plugin插件,作用是添加模板到编译完成后的dist的文件里面
var ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
//引入extract-text-webpack-plugin插件，作用是把css文件单独存为一个文件，如果不用插件，css会以style标签的方式插入html中


module.exports = {
    // 入口文件配置
    entry: {
        // 多页配置
        index: './src/js/index.js',
        login: './src/js/login.js'
    },
    //出口文件的配置项
    output: {
        path: path.resolve(__dirname, 'dist'),
        //输出的文件名称
        filename: './js/[name].js'
    },


    //插件，用于生产模版和各项功能
    plugins: [
        new ExtractTextWebpackPlugin({
            filename: "css/[name].css",//制定编译后的目录
            allChunks: true,//把分割的块分别打包
        }),
        //配置html模板，因为是多页面，所以需配置多个模板

        new HtmlWebpackPlugin({
            filename: './html/index.html',
            //文件目录名
            template: './src/view/index.html',
            //文件模板目录
            hash: true,
            //是否添加hash值
            chunks: ['index'],
            //模板需要引用的js块，vendors是定义的公共块，index是引用的自己编写的块
        }),
        new HtmlWebpackPlugin({
            filename: './html/login.html',
            template: './src/view/login.html',
            hash: true,
            chunks: ['login'],
        }),



    ],

    // 配置webpack开发服务功能
    // devServer:{}

    // 模块：例如解读CSS,图片如何转换，压缩
    module: {
        rules: [
            { test: /\.css/, use: ExtractTextWebpackPlugin.extract({ use: ['css-loader'] }) },
            // 带css的css编译 -> 自动匹配名字
            { test: /\.scss/, use: ExtractTextWebpackPlugin.extract({ fallback: "style-loader", use: ['css-loader', 'sass-loader'] }) },
            // 带scss的css编译
            { test: /\.(svg|jpg|jpeg|gif|woff|woff2|eot|ttf|otf)$/, use: [{ loader: 'file-loader', options: { outputPath: 'assets/' } }] },
            // 图片和字体加载
            { test: /\.png$/, use: { loader: "url-loader", options: { mimetype: "image/png", limit: "4096" } } },
            // 如果有png格式的图片，超过4M直接转化为base64格式
            {
                test: /\.html$/, use: {
                    loader: 'html-loader', options: {           //打包html文件
                        minimize: true,
                        // 是否打包为最小值
                        removeComments: true,
                        // 是否移除注释
                        collapseWhitespace: false,
                        // 是否合并空格
                    }
                }
            },


        ],


    }

}